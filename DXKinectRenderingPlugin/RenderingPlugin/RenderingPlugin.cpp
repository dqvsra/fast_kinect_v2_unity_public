//renders the body index frame to a texture. can be extended to write the color frame easily... I just didn't need it.

#include "UnityPluginInterface.h"
#define BOOST_DATE_TIME_NO_LIB
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <string>

// --------------------------------------------------------------------------
// Include headers for the graphics APIs we support

#if SUPPORT_D3D9
	#include <d3d9.h>
#endif
#if SUPPORT_D3D11
	#include <d3d11.h>
#endif
#if SUPPORT_OPENGL
	#if UNITY_WIN
		#include <gl/GL.h>
	#else
		#include <OpenGL/OpenGL.h>
	#endif
#endif



// --------------------------------------------------------------------------
// Helper function to print a string

extern "C" void EXPORT_API DebugLog(const char* str)
{
	#if UNITY_WIN
	//OutputDebugStringA (str);
	//freopen("./Assets/KinectRenderingPluginDebug.txt", "a", stdout);
	//printf("%s\n", str);
	//fclose(stdout);
	
	#else
	printf ("%s", str);
	#endif
}

// COM-like Release macro
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(a) if (a) { a->Release(); a = NULL; }
#endif



#define IsFlag( Flag, data ) (((Flag)&(data))==(data))          
#define NUMELEMENTS(x)  (sizeof(x) / sizeof(x[0]))


static std::string _sprintf(const char* format, ...)
{
	va_list args;
	char buf[1024];

	va_start(args, format);
	vsprintf_s(buf, format, args);

	return std::string(buf);
}




std::string DebugStringDXGI(DXGI_FORMAT fmt)
{
	std::string ret;
	switch (fmt)
	{
	case DXGI_FORMAT_UNKNOWN:               { ret = "UNKNOWN"; }break;

	case DXGI_FORMAT_R32G32B32A32_TYPELESS: { ret = "R32G32B32A32_TYPELESS"; }break;
	case DXGI_FORMAT_R32G32B32A32_FLOAT:    { ret = "R32G32B32A32_FLOAT"; }break;
	case DXGI_FORMAT_R32G32B32A32_UINT:     { ret = "R32G32B32A32_UINT"; }break;
	case DXGI_FORMAT_R32G32B32A32_SINT:     { ret = "R32G32B32A32_SINT"; }break;

	case DXGI_FORMAT_R32G32B32_TYPELESS:    { ret = "R32G32B32_TYPELESS"; }break;
	case DXGI_FORMAT_R32G32B32_FLOAT:       { ret = "R32G32B32_FLOAT"; }break;
	case DXGI_FORMAT_R32G32B32_UINT:        { ret = "R32G32B32_UINT"; }break;
	case DXGI_FORMAT_R32G32B32_SINT:        { ret = "R32G32B32_SINT"; }break;

	case DXGI_FORMAT_R16G16B16A16_TYPELESS: { ret = "R16G16B16A16_TYPELESS"; }break;
	case DXGI_FORMAT_R16G16B16A16_FLOAT:    { ret = "R16G16B16A16_FLOAT"; }break;
	case DXGI_FORMAT_R16G16B16A16_UNORM:    { ret = "R16G16B16A16_UNORM"; }break;
	case DXGI_FORMAT_R16G16B16A16_UINT:     { ret = "R16G16B16A16_UINT"; }break;
	case DXGI_FORMAT_R16G16B16A16_SNORM:    { ret = "R16G16B16A16_SNORM"; }break;
	case DXGI_FORMAT_R16G16B16A16_SINT:     { ret = "R16G16B16A16_SINT"; }break;
	case DXGI_FORMAT_R32G32_TYPELESS:       { ret = "R32G32_TYPELESS"; }break;
	case DXGI_FORMAT_R32G32_FLOAT:          { ret = "R32G32_FLOAT"; }break;
	case DXGI_FORMAT_R32G32_UINT:           { ret = "R32G32_UINT"; }break;
	case DXGI_FORMAT_R32G32_SINT:           { ret = "R32G32_SINT"; }break;
	case DXGI_FORMAT_R32G8X24_TYPELESS:     { ret = "R32G8X24_TYPELESS"; }break;
	case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:  { ret = "D32_FLOAT_S8X24_UINT"; }break;
	case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS: { ret = "R32_FLOAT_X8X24_TYPELESS"; }break;
	case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT: { ret = "X32_TYPELESS_G8X24_UINT"; }break;

	case DXGI_FORMAT_R10G10B10A2_TYPELESS:{ ret = "R10G10B10A2_TYPELESS"; }break;
	case DXGI_FORMAT_R10G10B10A2_UNORM:   { ret = "R10G10B10A2_UNORM"; }break;
	case DXGI_FORMAT_R10G10B10A2_UINT:    { ret = "R10G10B10A2_UINT"; }break;
	case DXGI_FORMAT_R11G11B10_FLOAT:     { ret = "R11G11B10_FLOAT"; }break;
	case DXGI_FORMAT_R8G8B8A8_TYPELESS:   { ret = "R8G8B8A8_TYPELESS"; }break;
	case DXGI_FORMAT_R8G8B8A8_UNORM:      { ret = "R8G8B8A8_UNORM"; }break;
	case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB: { ret = "R8G8B8A8_UNORM_SRGB"; }break;
	case DXGI_FORMAT_R8G8B8A8_UINT:       { ret = "R8G8B8A8_UINT"; }break;
	case DXGI_FORMAT_R8G8B8A8_SNORM:      { ret = "R8G8B8A8_SNORM"; }break;
	case DXGI_FORMAT_R8G8B8A8_SINT:       { ret = "R8G8B8A8_SINT"; }break;
	case DXGI_FORMAT_R16G16_TYPELESS:     { ret = "R16G16_TYPELESS"; }break;
	case DXGI_FORMAT_R16G16_FLOAT:        { ret = "R16G16_FLOAT"; }break;
	case DXGI_FORMAT_R16G16_UNORM:        { ret = "R16G16_UNORM"; }break;
	case DXGI_FORMAT_R16G16_UINT:         { ret = "R16G16_UINT"; }break;
	case DXGI_FORMAT_R16G16_SNORM:        { ret = "R16G16_SNORM"; }break;
	case DXGI_FORMAT_R16G16_SINT:         { ret = "R16G16_SINT"; }break;
	case DXGI_FORMAT_R32_TYPELESS:        { ret = "R32_TYPELESS"; }break;
	case DXGI_FORMAT_D32_FLOAT:           { ret = "D32_FLOAT"; }break;
	case DXGI_FORMAT_R32_FLOAT:           { ret = "R32_FLOAT"; }break;
	case DXGI_FORMAT_R32_UINT:            { ret = "R32_UINT"; }break;
	case DXGI_FORMAT_R32_SINT:            { ret = "R32_SINT"; }break;
	case DXGI_FORMAT_R24G8_TYPELESS:      { ret = "R24G8_TYPELESS"; }break;
	case DXGI_FORMAT_D24_UNORM_S8_UINT:   { ret = "D24_UNORM_S8_UINT"; }break;
	case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:{ ret = "R24_UNORM_X8_TYPELESS"; }break;
	case DXGI_FORMAT_X24_TYPELESS_G8_UINT:{ ret = "X24_TYPELESS_G8_UINT"; }break;

	case DXGI_FORMAT_R8G8_TYPELESS: { ret = "R8G8_TYPELESS"; }break;
	case DXGI_FORMAT_R8G8_UNORM:    { ret = "R8G8_UNORM"; }break;
	case DXGI_FORMAT_R8G8_UINT:     { ret = "R8G8_UINT"; }break;
	case DXGI_FORMAT_R8G8_SNORM:    { ret = "R8G8_SNORM"; }break;
	case DXGI_FORMAT_R8G8_SINT:     { ret = "R8G8_SINT"; }break;
	case DXGI_FORMAT_R16_TYPELESS:  { ret = "R16_TYPELESS"; }break;
	case DXGI_FORMAT_R16_FLOAT:     { ret = "R16_FLOAT"; }break;
	case DXGI_FORMAT_D16_UNORM:     { ret = "D16_UNORM"; }break;
	case DXGI_FORMAT_R16_UNORM:     { ret = "R16_UNORM"; }break;
	case DXGI_FORMAT_R16_UINT:      { ret = "R16_UINT"; }break;
	case DXGI_FORMAT_R16_SNORM:     { ret = "R16_SNORM"; }break;
	case DXGI_FORMAT_R16_SINT:      { ret = "R16_SINT"; }break;

	case DXGI_FORMAT_R8_TYPELESS: { ret = "R8_TYPELESS"; }break;
	case DXGI_FORMAT_R8_UNORM:    { ret = "R8_UNORM"; }break;
	case DXGI_FORMAT_R8_UINT:     { ret = "R8_UINT"; }break;
	case DXGI_FORMAT_R8_SNORM:    { ret = "R8_SNORM"; }break;
	case DXGI_FORMAT_R8_SINT:     { ret = "R8_SINT"; }break;
	case DXGI_FORMAT_A8_UNORM:    { ret = "A8_UNORM"; }break;

	case DXGI_FORMAT_R1_UNORM:    { ret = "R1_UNORM"; }break;

	case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:{ ret = "R9G9B9E5_SHAREDEXP"; }break;
	case DXGI_FORMAT_R8G8_B8G8_UNORM:   { ret = "R8G8_B8G8_UNORM"; }break;
	case DXGI_FORMAT_G8R8_G8B8_UNORM:   { ret = "G8R8_G8B8_UNORM"; }break;

	case DXGI_FORMAT_BC1_TYPELESS:  { ret = "BC1_TYPELESS"; }break;
	case DXGI_FORMAT_BC1_UNORM:     { ret = "BC1_UNORM"; }break;
	case DXGI_FORMAT_BC1_UNORM_SRGB:{ ret = "BC1_UNORM_SRGB"; }break;
	case DXGI_FORMAT_BC2_TYPELESS:  { ret = "BC2_TYPELESS"; }break;
	case DXGI_FORMAT_BC2_UNORM:     { ret = "BC2_UNORM"; }break;
	case DXGI_FORMAT_BC2_UNORM_SRGB:{ ret = "BC2_UNORM_SRGB"; }break;
	case DXGI_FORMAT_BC3_TYPELESS:  { ret = "BC3_TYPELESS"; }break;
	case DXGI_FORMAT_BC3_UNORM:     { ret = "BC3_UNORM"; }break;
	case DXGI_FORMAT_BC3_UNORM_SRGB:{ ret = "BC3_UNORM_SRGB"; }break;
	case DXGI_FORMAT_BC4_TYPELESS:  { ret = "BC4_TYPELESS"; }break;
	case DXGI_FORMAT_BC4_UNORM:     { ret = "BC4_UNORM"; }break;
	case DXGI_FORMAT_BC4_SNORM:     { ret = "BC4_SNORM"; }break;
	case DXGI_FORMAT_BC5_TYPELESS:  { ret = "BC5_TYPELESS"; }break;
	case DXGI_FORMAT_BC5_UNORM:     { ret = "BC5_UNORM"; }break;
	case DXGI_FORMAT_BC5_SNORM:     { ret = "BC5_SNORM"; }break;

	case DXGI_FORMAT_B5G6R5_UNORM:  { ret = "B5G6R5_UNORM"; }break;
	case DXGI_FORMAT_B5G5R5A1_UNORM:{ ret = "B5G5R5A1_UNORM"; }break;
	case DXGI_FORMAT_B8G8R8A8_UNORM:{ ret = "B8G8R8A8_UNORM"; }break;
	case DXGI_FORMAT_B8G8R8X8_UNORM:{ ret = "B8G8R8X8_UNORM"; }break;

	case DXGI_FORMAT_FORCE_UINT: { ret = "FORCE_UINT"; }break;
	}

	ret += _sprintf("(%0d)", fmt);
	return ret;
}




std::string DebugStringDXGI(const DXGI_SWAP_CHAIN_DESC& desc)
{
	/*
	desc.BufferDesc.Width  = m_Window.GetClientSize().w;
	desc.BufferDesc.Height = m_Window.GetClientSize().h;
	desc.BufferDesc.RefreshRate.Numerator = 60;
	desc.BufferDesc.RefreshRate.Denominator = 1;
	desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
	desc.BufferDesc.Scaling = DXGI_MODE_SCALING_STRETCHED;
	desc.BufferCount = 1;

	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;

	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.OutputWindow = m_Window.GetHWND();
	desc.Windowed = TRUE;
	desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	*/

	return std::string();
}

std::string DebugStringD3D10(BOOL b)
{
	std::string ret;

	if (b == TRUE) { ret = "TRUE"; }
	else          { ret = "FALSE"; }

	ret += _sprintf("(%0x)", b);
	return ret;
}


std::string DebugStringD3D10(D3D10_BLEND type)
{
	std::string ret;

	switch (type)
	{
	case D3D10_BLEND_ZERO: { ret = "ZERO"; }break;
	case D3D10_BLEND_ONE:  { ret = "ONE"; }break;
	case D3D10_BLEND_SRC_COLOR:       { ret = "SRC_COLOR";      }break;
	case D3D10_BLEND_INV_SRC_COLOR:   { ret = "INV_SRC_COLOR";  }break;
	case D3D10_BLEND_SRC_ALPHA:       { ret = "SRC_ALPHA";      }break;
	case D3D10_BLEND_INV_SRC_ALPHA:   { ret = "INV_SRC_ALPHA";  }break;
	case D3D10_BLEND_DEST_ALPHA:      { ret = "DEST_ALPHA";     }break;
	case D3D10_BLEND_INV_DEST_ALPHA:  { ret = "INV_DEST_ALPHA"; }break;
	case D3D10_BLEND_DEST_COLOR:      { ret = "DEST_COLOR";     }break;
	case D3D10_BLEND_INV_DEST_COLOR:  { ret = "INV_DEST_COLOR"; }break;
	case D3D10_BLEND_SRC_ALPHA_SAT:   { ret = "SRC_ALPHA_SAT";  }break;
	case D3D10_BLEND_BLEND_FACTOR:    { ret = "BLEND_FACTOR";   }break;
	case D3D10_BLEND_INV_BLEND_FACTOR:{ ret = "INV_BLEND_FACTOR"; }break;
	case D3D10_BLEND_SRC1_COLOR:      { ret = "SRC1_COLOR";     }break;
	case D3D10_BLEND_INV_SRC1_COLOR:  { ret = "INV_SRC1_COLOR"; }break;
	case D3D10_BLEND_SRC1_ALPHA:      { ret = "SRC1_ALPHA";     }break;
	case D3D10_BLEND_INV_SRC1_ALPHA:  { ret = "INV_SRC1_ALPHA"; }break;
	}

	ret += _sprintf("(%0x)", type);
	return ret;
}

std::string DebugStringD3D10(D3D10_BLEND_OP type)
{
	std::string ret;

	switch (type)
	{
	case D3D10_BLEND_OP_ADD:          { ret = "ADD"; }break;
	case D3D10_BLEND_OP_SUBTRACT:     { ret = "SUBTRACT"; }break;
	case D3D10_BLEND_OP_REV_SUBTRACT: { ret = "REV_SUBTRACT"; }break;
	case D3D10_BLEND_OP_MIN:          { ret = "MIN"; }break;
	case D3D10_BLEND_OP_MAX:          { ret = "MAX"; }break;
	}

	ret += _sprintf("(%0x)", type);
	return ret;
}


std::string DebugStringD3D10(D3D10_USAGE usage)
{
	std::string ret;

	switch (usage)
	{
	case D3D10_USAGE_DEFAULT:   { ret = "DEFAULT"; }break;
	case D3D10_USAGE_IMMUTABLE: { ret = "IMMUTABLE"; }break;
	case D3D10_USAGE_DYNAMIC:   { ret = "DYNAMIC"; }break;
	case D3D10_USAGE_STAGING:   { ret = "STAGING"; }break;
	}
	ret += _sprintf("(%0x)", usage);

	return ret;
}

std::string DebugStringD3D10(D3D10_BIND_FLAG flag)
{
	std::string ret;

	if (IsFlag(flag, D3D10_BIND_VERTEX_BUFFER)) { ret += "+VERTEX_BUFFER"; }
	if (IsFlag(flag, D3D10_BIND_INDEX_BUFFER)) { ret += "+INDEX_BUFFER"; }
	if (IsFlag(flag, D3D10_BIND_CONSTANT_BUFFER)) { ret += "+CONSTANT_BUFFER"; }
	if (IsFlag(flag, D3D10_BIND_SHADER_RESOURCE)) { ret += "+SHADER_RESOURCE"; }
	if (IsFlag(flag, D3D10_BIND_STREAM_OUTPUT)) { ret += "+STREAM_OUTPUT"; }
	if (IsFlag(flag, D3D10_BIND_RENDER_TARGET)) { ret += "+RENDER_TARGET"; }
	if (IsFlag(flag, D3D10_BIND_DEPTH_STENCIL)) { ret += "+DEPTH_STENCIL"; }

	ret += _sprintf("(%0x)", flag);

	return ret;
}

std::string DebugStringD3D10(D3D10_CPU_ACCESS_FLAG flag)
{
	std::string ret;

	if (IsFlag(flag, D3D10_CPU_ACCESS_WRITE)) { ret += "+WRITE"; }
	if (IsFlag(flag, D3D10_CPU_ACCESS_READ)) { ret += "+READ"; }

	ret += _sprintf("(%0x)", flag);

	return ret;
}

std::string DebugStringD3D10(D3D10_RESOURCE_MISC_FLAG flag)
{
	std::string ret;

	if (IsFlag(flag, D3D10_RESOURCE_MISC_GENERATE_MIPS)) { ret += "+GENERATE_MIPS"; }
	if (IsFlag(flag, D3D10_RESOURCE_MISC_SHARED)) { ret += "+SHARED"; }
	if (IsFlag(flag, D3D10_RESOURCE_MISC_TEXTURECUBE)) { ret += "+TEXTURECUBE"; }
	//  if( IsFlag(flag,D3D10_RESOURCE_MISC_SHARED_KEYEDMUTEX ) ) { ret+="+SHARED_KEYEDMUTEX";  }
	//  if( IsFlag(flag,D3D10_RESOURCE_MISC_GDI_COMPATIBLE    ) ) { ret+="+GDI_COMPATIBLE";  }

	ret += _sprintf("(%0x)", flag);

	return ret;
}

std::string DebugStringD3D10(D3D10_MAP type)
{
	std::string ret;

	switch (type)
	{
	case D3D10_MAP_READ:          { ret = "READ"; }break;
	case D3D10_MAP_WRITE:         { ret = "WRITE"; }break;
	case D3D10_MAP_READ_WRITE:    { ret = "READ_WRITE"; }break;
	case D3D10_MAP_WRITE_DISCARD: { ret = "WRITE_DISCARD"; }break;
	case D3D10_MAP_WRITE_NO_OVERWRITE:{ ret = "WRITE_NO_OVERWRITE"; }break;
	}
	ret += _sprintf("(%0x)", type);

	return ret;

}



std::string DebugStringD3D10(const D3D10_TEXTURE2D_DESC& desc)
{
	std::string ret;

	ret += _sprintf(" Width:%0d", desc.Width);
	ret += _sprintf(" Height:%0d", desc.Height);
	ret += _sprintf(" MipLevels:%0d", desc.MipLevels);
	ret += _sprintf(" ArraySize:%0d", desc.ArraySize);
	ret += _sprintf(" Format:%s", DebugStringDXGI(desc.Format).c_str());
	ret += _sprintf(" SampleCount:%0d", desc.SampleDesc.Count);
	ret += _sprintf(" SampleQuality:%0d", desc.SampleDesc.Quality);
	ret += _sprintf(" Usage:%s", DebugStringD3D10(desc.Usage).c_str());
	ret += _sprintf(" BindFlags:%s", DebugStringD3D10((D3D10_BIND_FLAG)desc.BindFlags).c_str());
	ret += _sprintf(" CPUAccessFlags:%s", DebugStringD3D10((D3D10_CPU_ACCESS_FLAG)desc.CPUAccessFlags).c_str());
	ret += _sprintf(" MiscFlags:%s", DebugStringD3D10((D3D10_RESOURCE_MISC_FLAG)desc.MiscFlags).c_str());

	return ret;
}

std::string DebugStringD3D10(const D3D10_SUBRESOURCE_DATA* desc)
{
	if (desc == NULL) { return "NULL"; }
	std::string ret;

	ret += _sprintf(" pSysMem:%0x", desc->pSysMem);
	ret += _sprintf(" SysMemPitch:%0d", desc->SysMemPitch);
	ret += _sprintf(" SysMemSlicePitch:%0d", desc->SysMemSlicePitch);

	return ret;
}




std::string DebugStringD3D10(const D3D10_BLEND_DESC& desc)
{
	std::string ret;

	ret += _sprintf(" AlphaToCoverageEnable:%s", DebugStringD3D10(desc.AlphaToCoverageEnable).c_str());
	ret += _sprintf(" BlendEnable[0]:%s", DebugStringD3D10(desc.BlendEnable[0]).c_str());
	ret += _sprintf(" BlendEnable[1]:%s", DebugStringD3D10(desc.BlendEnable[1]).c_str());
	ret += _sprintf(" BlendEnable[2]:%s", DebugStringD3D10(desc.BlendEnable[2]).c_str());
	ret += _sprintf(" BlendEnable[3]:%s", DebugStringD3D10(desc.BlendEnable[3]).c_str());
	ret += _sprintf(" BlendEnable[4]:%s", DebugStringD3D10(desc.BlendEnable[4]).c_str());
	ret += _sprintf(" BlendEnable[5]:%s", DebugStringD3D10(desc.BlendEnable[5]).c_str());
	ret += _sprintf(" BlendEnable[6]:%s", DebugStringD3D10(desc.BlendEnable[6]).c_str());
	ret += _sprintf(" BlendEnable[7]:%s", DebugStringD3D10(desc.BlendEnable[7]).c_str());

	ret += _sprintf(" SrcBlend:%s", DebugStringD3D10(desc.SrcBlend).c_str());
	ret += _sprintf(" DestBlend:%s", DebugStringD3D10(desc.DestBlend).c_str());
	ret += _sprintf(" BlendOp:%s", DebugStringD3D10(desc.BlendOp).c_str());
	ret += _sprintf(" SrcBlendAlpha:%s", DebugStringD3D10(desc.SrcBlendAlpha).c_str());
	ret += _sprintf(" DestBlendAlpha:%s", DebugStringD3D10(desc.DestBlendAlpha).c_str());
	ret += _sprintf(" BlendOpAlpha:%s", DebugStringD3D10(desc.BlendOpAlpha).c_str());
	ret += _sprintf(" RenderTargetWriteMask[0]:%x", desc.RenderTargetWriteMask[0]);
	ret += _sprintf(" RenderTargetWriteMask[1]:%x", desc.RenderTargetWriteMask[1]);
	ret += _sprintf(" RenderTargetWriteMask[2]:%x", desc.RenderTargetWriteMask[2]);
	ret += _sprintf(" RenderTargetWriteMask[3]:%x", desc.RenderTargetWriteMask[3]);
	ret += _sprintf(" RenderTargetWriteMask[4]:%x", desc.RenderTargetWriteMask[4]);
	ret += _sprintf(" RenderTargetWriteMask[5]:%x", desc.RenderTargetWriteMask[5]);
	ret += _sprintf(" RenderTargetWriteMask[6]:%x", desc.RenderTargetWriteMask[6]);
	ret += _sprintf(" RenderTargetWriteMask[7]:%x", desc.RenderTargetWriteMask[7]);

	return ret;
}


std::string DebugStringD3D10(const D3D10_BUFFER_DESC& desc)
{
	std::string ret;

	ret += _sprintf(" ByteWidth:%0d", desc.ByteWidth);
	ret += _sprintf(" Usage:%s", DebugStringD3D10(desc.Usage).c_str());
	ret += _sprintf(" BindFlags:%s", DebugStringD3D10((D3D10_BIND_FLAG)desc.BindFlags).c_str());
	ret += _sprintf(" CPUAccessFlags:%s", DebugStringD3D10((D3D10_CPU_ACCESS_FLAG)desc.CPUAccessFlags).c_str());
	ret += _sprintf(" MiscFlags:%s", DebugStringD3D10((D3D10_RESOURCE_MISC_FLAG)desc.MiscFlags).c_str());

	return ret;
}





std::string DebugStringD3D11(BOOL b)
{
	std::string ret;

	if (b == TRUE) { ret = "TRUE"; }
	else          { ret = "FALSE"; }

	ret += _sprintf("(%0x)", b);
	return ret;
}


std::string DebugStringD3D11(D3D11_BLEND type)
{
	std::string ret;

	switch (type)
	{
	case D3D11_BLEND_ZERO: { ret = "ZERO"; }break;
	case D3D11_BLEND_ONE:  { ret = "ONE"; }break;
	case D3D11_BLEND_SRC_COLOR:       { ret = "SRC_COLOR";      }break;
	case D3D11_BLEND_INV_SRC_COLOR:   { ret = "INV_SRC_COLOR";  }break;
	case D3D11_BLEND_SRC_ALPHA:       { ret = "SRC_ALPHA";      }break;
	case D3D11_BLEND_INV_SRC_ALPHA:   { ret = "INV_SRC_ALPHA";  }break;
	case D3D11_BLEND_DEST_ALPHA:      { ret = "DEST_ALPHA";     }break;
	case D3D11_BLEND_INV_DEST_ALPHA:  { ret = "INV_DEST_ALPHA"; }break;
	case D3D11_BLEND_DEST_COLOR:      { ret = "DEST_COLOR";     }break;
	case D3D11_BLEND_INV_DEST_COLOR:  { ret = "INV_DEST_COLOR"; }break;
	case D3D11_BLEND_SRC_ALPHA_SAT:   { ret = "SRC_ALPHA_SAT";  }break;
	case D3D11_BLEND_BLEND_FACTOR:    { ret = "BLEND_FACTOR";   }break;
	case D3D11_BLEND_INV_BLEND_FACTOR:{ ret = "INV_BLEND_FACTOR"; }break;
	case D3D11_BLEND_SRC1_COLOR:      { ret = "SRC1_COLOR";     }break;
	case D3D11_BLEND_INV_SRC1_COLOR:  { ret = "INV_SRC1_COLOR"; }break;
	case D3D11_BLEND_SRC1_ALPHA:      { ret = "SRC1_ALPHA";     }break;
	case D3D11_BLEND_INV_SRC1_ALPHA:  { ret = "INV_SRC1_ALPHA"; }break;
	}

	ret += _sprintf("(%0x)", type);
	return ret;
}

std::string DebugStringD3D11(D3D11_BLEND_OP type)
{
	std::string ret;

	switch (type)
	{
	case D3D11_BLEND_OP_ADD:          { ret = "ADD"; }break;
	case D3D11_BLEND_OP_SUBTRACT:     { ret = "SUBTRACT"; }break;
	case D3D11_BLEND_OP_REV_SUBTRACT: { ret = "REV_SUBTRACT"; }break;
	case D3D11_BLEND_OP_MIN:          { ret = "MIN"; }break;
	case D3D11_BLEND_OP_MAX:          { ret = "MAX"; }break;
	}

	ret += _sprintf("(%0x)", type);
	return ret;
}


std::string DebugStringD3D11(D3D11_USAGE usage)
{
	std::string ret;

	switch (usage)
	{
	case D3D11_USAGE_DEFAULT:   { ret = "DEFAULT"; }break;
	case D3D11_USAGE_IMMUTABLE: { ret = "IMMUTABLE"; }break;
	case D3D11_USAGE_DYNAMIC:   { ret = "DYNAMIC"; }break;
	case D3D11_USAGE_STAGING:   { ret = "STAGING"; }break;
	}
	ret += _sprintf("(%0x)", usage);

	return ret;
}

std::string DebugStringD3D11(D3D11_BIND_FLAG flag)
{
	std::string ret;

	if (IsFlag(flag, D3D11_BIND_VERTEX_BUFFER)) { ret += "+VERTEX_BUFFER"; }
	if (IsFlag(flag, D3D11_BIND_INDEX_BUFFER)) { ret += "+INDEX_BUFFER"; }
	if (IsFlag(flag, D3D11_BIND_CONSTANT_BUFFER)) { ret += "+CONSTANT_BUFFER"; }
	if (IsFlag(flag, D3D11_BIND_SHADER_RESOURCE)) { ret += "+SHADER_RESOURCE"; }
	if (IsFlag(flag, D3D11_BIND_STREAM_OUTPUT)) { ret += "+STREAM_OUTPUT"; }
	if (IsFlag(flag, D3D11_BIND_RENDER_TARGET)) { ret += "+RENDER_TARGET"; }
	if (IsFlag(flag, D3D11_BIND_DEPTH_STENCIL)) { ret += "+DEPTH_STENCIL"; }

	ret += _sprintf("(%0x)", flag);

	return ret;
}

std::string DebugStringD3D11(D3D11_CPU_ACCESS_FLAG flag)
{
	std::string ret;

	if (IsFlag(flag, D3D11_CPU_ACCESS_WRITE)) { ret += "+WRITE"; }
	if (IsFlag(flag, D3D11_CPU_ACCESS_READ)) { ret += "+READ"; }

	ret += _sprintf("(%0x)", flag);

	return ret;
}

std::string DebugStringD3D11(D3D11_RESOURCE_MISC_FLAG flag)
{
	std::string ret;

	if (IsFlag(flag, D3D11_RESOURCE_MISC_GENERATE_MIPS)) { ret += "+GENERATE_MIPS"; }
	if (IsFlag(flag, D3D11_RESOURCE_MISC_SHARED)) { ret += "+SHARED"; }
	if (IsFlag(flag, D3D11_RESOURCE_MISC_TEXTURECUBE)) { ret += "+TEXTURECUBE"; }
	//  if( IsFlag(flag,D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX ) ) { ret+="+SHARED_KEYEDMUTEX";  }
	//  if( IsFlag(flag,D3D11_RESOURCE_MISC_GDI_COMPATIBLE    ) ) { ret+="+GDI_COMPATIBLE";  }

	ret += _sprintf("(%0x)", flag);

	return ret;
}

std::string DebugStringD3D11(D3D11_MAP type)
{
	std::string ret;

	switch (type)
	{
	case D3D11_MAP_READ:          { ret = "READ"; }break;
	case D3D11_MAP_WRITE:         { ret = "WRITE"; }break;
	case D3D11_MAP_READ_WRITE:    { ret = "READ_WRITE"; }break;
	case D3D11_MAP_WRITE_DISCARD: { ret = "WRITE_DISCARD"; }break;
	case D3D11_MAP_WRITE_NO_OVERWRITE:{ ret = "WRITE_NO_OVERWRITE"; }break;
	}
	ret += _sprintf("(%0x)", type);

	return ret;
}

std::string DebugStringD3D11(D3D11_INPUT_CLASSIFICATION type)
{
	std::string ret;

	switch (type)
	{
	case D3D11_INPUT_PER_VERTEX_DATA:          { ret = "PER_VERTEX"; }break;
	case D3D11_INPUT_PER_INSTANCE_DATA:         { ret = "PER_INSTANCE"; }break;
	}
	ret += _sprintf("(%0x)", type);

	return ret;
}



static std::string DebugStringD3D11(const D3D11_TEXTURE2D_DESC& desc)
{
	std::string ret;

	ret += _sprintf(" Width:%0d", desc.Width);
	ret += _sprintf(" Height:%0d", desc.Height);
	ret += _sprintf(" MipLevels:%0d", desc.MipLevels);
	ret += _sprintf(" ArraySize:%0d", desc.ArraySize);
	ret += _sprintf(" Format:%s", DebugStringDXGI(desc.Format).c_str());
	ret += _sprintf(" SampleCount:%0d", desc.SampleDesc.Count);
	ret += _sprintf(" SampleQuality:%0d", desc.SampleDesc.Quality);
	ret += _sprintf(" Usage:%s", DebugStringD3D11(desc.Usage).c_str());
	ret += _sprintf(" BindFlags:%s", DebugStringD3D11((D3D11_BIND_FLAG)desc.BindFlags).c_str());
	ret += _sprintf(" CPUAccessFlags:%s", DebugStringD3D11((D3D11_CPU_ACCESS_FLAG)desc.CPUAccessFlags).c_str());
	ret += _sprintf(" MiscFlags:%s", DebugStringD3D11((D3D11_RESOURCE_MISC_FLAG)desc.MiscFlags).c_str());

	return ret;
}

std::string DebugStringD3D11(const D3D11_SUBRESOURCE_DATA* desc)
{
	if (desc == NULL) { return "NULL"; }
	std::string ret;

	ret += _sprintf(" pSysMem:%0x", desc->pSysMem);
	ret += _sprintf(" SysMemPitch:%0d", desc->SysMemPitch);
	ret += _sprintf(" SysMemSlicePitch:%0d", desc->SysMemSlicePitch);

	return ret;
}




std::string DebugStringD3D11(const D3D11_BLEND_DESC& desc)
{
	std::string ret;

	ret += _sprintf(" AlphaToCoverageEnable:%s", DebugStringD3D11(desc.AlphaToCoverageEnable).c_str());

	for (int i = 0; i<NUMELEMENTS(desc.RenderTarget); ++i)
	{
		const D3D11_RENDER_TARGET_BLEND_DESC& rt = desc.RenderTarget[i];

		ret += _sprintf(" rt[%0d].BlendEnable:%s", i, DebugStringD3D11(rt.BlendEnable).c_str());
		ret += _sprintf(" rt[%0d].SrcBlend:%s", i, DebugStringD3D11(rt.SrcBlend).c_str());
		ret += _sprintf(" rt[%0d].DestBlend:%s", i, DebugStringD3D11(rt.DestBlend).c_str());
		ret += _sprintf(" rt[%0d].BlendOp:%s", i, DebugStringD3D11(rt.BlendOp).c_str());
		ret += _sprintf(" rt[%0d].SrcBlendAlpha:%s", i, DebugStringD3D11(rt.SrcBlendAlpha).c_str());
		ret += _sprintf(" rt[%0d].DestBlendAlpha:%s", i, DebugStringD3D11(rt.DestBlendAlpha).c_str());
		ret += _sprintf(" rt[%0d].BlendOpAlpha:%s", i, DebugStringD3D11(rt.BlendOpAlpha).c_str());
		ret += _sprintf(" rt[%0d].RenderTargetWriteMask:%x", i, rt.RenderTargetWriteMask);
	}


	return ret;
}


std::string DebugStringD3D11(const D3D11_BUFFER_DESC& desc)
{
	std::string ret;

	ret += _sprintf(" ByteWidth:%0d", desc.ByteWidth);
	ret += _sprintf(" Usage:%s", DebugStringD3D11(desc.Usage).c_str());
	ret += _sprintf(" BindFlags:%s", DebugStringD3D11((D3D11_BIND_FLAG)desc.BindFlags).c_str());
	ret += _sprintf(" CPUAccessFlags:%s", DebugStringD3D11((D3D11_CPU_ACCESS_FLAG)desc.CPUAccessFlags).c_str());
	ret += _sprintf(" MiscFlags:%s", DebugStringD3D11((D3D11_RESOURCE_MISC_FLAG)desc.MiscFlags).c_str());

	return ret;
}


std::string DebugStringD3D11(const D3D11_INPUT_ELEMENT_DESC& desc)
{
	std::string ret;
	ret += _sprintf(" SemanticName:%s", desc.SemanticName);
	ret += _sprintf(" SemanticIndex:%0d", desc.SemanticIndex);
	ret += _sprintf(" Format:%s", DebugStringDXGI(desc.Format).c_str());
	ret += _sprintf(" InputSlot:%0d", desc.InputSlot);
	ret += _sprintf(" AlignedByteOffset:%0d", desc.AlignedByteOffset);
	ret += _sprintf(" InputSlotClass:%s", DebugStringD3D11(desc.InputSlotClass).c_str());

	ret += _sprintf(" InstanceDataStepRate:%0d", desc.InstanceDataStepRate);
	return ret;
}

// --------------------------------------------------------------------------
// SetTimeFromUnity, an example function we export which is called by one of the scripts.

static float g_Time;

extern "C" void EXPORT_API SetTimeFromUnity (float t) { g_Time = t; }



static int g_DeviceType = -1;
static int depthWidth;
static int depthHeight;

static int bodyIndexFrameTexturePtr;
static char* bodyIndexFrameTexturePtrSourceDataLocation;

//unused- using ComputeBuffers/ComputeShaders instead for this texture
static int depthFrameTexturePtr;
static void* depthFrameTexturePtrSourceDataLocation;

static int colorWidth;
static int colorHeight;

static int colorFrameTexturePtr;
static void* colorFrameTexturePtrSourceDataLocation;


extern "C" int EXPORT_API getGfxType(){
	return g_DeviceType;
}



	
extern "C" void EXPORT_API SetupBodyIndexFrameTexture(int w, int h, int _texturePtr, void* _bodyIndexFrameTexturePtrSourceDataLocation)
{
	depthWidth = w;
	depthHeight = h;

	bodyIndexFrameTexturePtr = _texturePtr;
	bodyIndexFrameTexturePtrSourceDataLocation =(char*) _bodyIndexFrameTexturePtrSourceDataLocation;
}

extern "C" void EXPORT_API CleanupBodyIndexFrameTexture(){
	depthWidth = 0;
	depthHeight = 0;
	bodyIndexFrameTexturePtr = 0;
	bodyIndexFrameTexturePtrSourceDataLocation = NULL;
}

//unused- using ComputeBuffers/ComputeShaders instead for this texture
extern "C" void EXPORT_API SetupDepthFrameTexture(int w, int h, int _texturePtr, void* _texturePtrSourceDataLocation)
{
	depthWidth = w;
	depthHeight = h;

	depthFrameTexturePtr = _texturePtr;
	depthFrameTexturePtrSourceDataLocation = _texturePtrSourceDataLocation;
}
extern "C" void EXPORT_API CleanupDepthFrameTexture(){
	depthWidth = 0;
	depthHeight = 0;
	depthFrameTexturePtr = 0;
	depthFrameTexturePtrSourceDataLocation = NULL;
}
//-------------------------------------------------------------------


// --------------------------------------------------------------------------
// UnitySetGraphicsDevice

// Actual setup/teardown functions defined below
#if SUPPORT_D3D9
static void SetGraphicsDeviceD3D9 (IDirect3DDevice9* device, GfxDeviceEventType eventType);
#endif
#if SUPPORT_D3D11
static void SetGraphicsDeviceD3D11 (ID3D11Device* device, GfxDeviceEventType eventType);
#endif
//


// -------------------------------------------------------------------
//  Direct3D 11 setup/teardown code


#if SUPPORT_D3D11

static ID3D11Device* g_D3D11Device;
static ID3D11Buffer* g_D3D11VB; // vertex buffer
static ID3D11Buffer* g_D3D11CB; // constant buffer
static ID3D11VertexShader* g_D3D11VertexShader;
static ID3D11PixelShader* g_D3D11PixelShader;
static ID3D11InputLayout* g_D3D11InputLayout;
static ID3D11RasterizerState* g_D3D11RasterState;
static ID3D11BlendState* g_D3D11BlendState;
static ID3D11DepthStencilState* g_D3D11DepthState;

typedef HRESULT (WINAPI *D3DCompileFunc)(
	const void* pSrcData,
	unsigned long SrcDataSize,
	const char* pFileName,
	const D3D10_SHADER_MACRO* pDefines,
	ID3D10Include* pInclude,
	const char* pEntrypoint,
	const char* pTarget,
	unsigned int Flags1,
	unsigned int Flags2,
	ID3D10Blob** ppCode,
	ID3D10Blob** ppErrorMsgs);

static const char* kD3D11ShaderText =
"cbuffer MyCB : register(b0) {\n"
"	float4x4 worldMatrix;\n"
"}\n"
"void VS (float3 pos : POSITION, float4 color : COLOR, out float4 ocolor : COLOR, out float4 opos : SV_Position) {\n"
"	opos = mul (worldMatrix, float4(pos,1));\n"
"	ocolor = color;\n"
"}\n"
"float4 PS (float4 color : COLOR) : SV_TARGET {\n"
"	return color;\n"
"}\n";


static void CreateD3D11Resources()
{
	D3D11_BUFFER_DESC desc;
	memset (&desc, 0, sizeof(desc));

	// vertex buffer
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.ByteWidth = 1024;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	g_D3D11Device->CreateBuffer (&desc, NULL, &g_D3D11VB);

	// constant buffer
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.ByteWidth = 64; // hold 1 matrix
	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.CPUAccessFlags = 0;
	g_D3D11Device->CreateBuffer (&desc, NULL, &g_D3D11CB);

	// shaders
	HMODULE compiler = LoadLibraryA("D3DCompiler_43.dll");

	if (compiler == NULL)
	{
		// Try compiler from Windows 8 SDK
		compiler = LoadLibraryA("D3DCompiler_47.dll");
	}
	if (compiler)
	{
		DebugLog("success loading the compiler.");
		ID3D10Blob* vsBlob = NULL;
		ID3D10Blob* psBlob = NULL;

		D3DCompileFunc compileFunc = (D3DCompileFunc)GetProcAddress (compiler, "D3DCompile");
		if (compileFunc)
		{
			HRESULT hr;
			hr = compileFunc(kD3D11ShaderText, strlen(kD3D11ShaderText), NULL, NULL, NULL, "VS", "vs_4_0", 0, 0, &vsBlob, NULL);
			if (SUCCEEDED(hr))
			{
				g_D3D11Device->CreateVertexShader (vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), NULL, &g_D3D11VertexShader);
			}

			hr = compileFunc(kD3D11ShaderText, strlen(kD3D11ShaderText), NULL, NULL, NULL, "PS", "ps_4_0", 0, 0, &psBlob, NULL);
			if (SUCCEEDED(hr))
			{
				g_D3D11Device->CreatePixelShader (psBlob->GetBufferPointer(), psBlob->GetBufferSize(), NULL, &g_D3D11PixelShader);
			}
		}


		// input layout
		if (g_D3D11VertexShader && vsBlob)
		{
			D3D11_INPUT_ELEMENT_DESC layout[] = {
				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
				{ "COLOR", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			};

			g_D3D11Device->CreateInputLayout (layout, 2, vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), &g_D3D11InputLayout);
		}

		SAFE_RELEASE(vsBlob);
		SAFE_RELEASE(psBlob);

		FreeLibrary (compiler);
	}
	else
	{
		DebugLog ("D3D11: HLSL shader compiler not found, will not render anything\n");
	}

	// render states
	D3D11_RASTERIZER_DESC rsdesc;
	memset (&rsdesc, 0, sizeof(rsdesc));
	rsdesc.FillMode = D3D11_FILL_SOLID;
	rsdesc.CullMode = D3D11_CULL_NONE;
	rsdesc.DepthClipEnable = TRUE;
	g_D3D11Device->CreateRasterizerState (&rsdesc, &g_D3D11RasterState);

	D3D11_DEPTH_STENCIL_DESC dsdesc;
	memset (&dsdesc, 0, sizeof(dsdesc));
	dsdesc.DepthEnable = TRUE;
	dsdesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsdesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	g_D3D11Device->CreateDepthStencilState (&dsdesc, &g_D3D11DepthState);

	D3D11_BLEND_DESC bdesc;
	memset (&bdesc, 0, sizeof(bdesc));
	bdesc.RenderTarget[0].BlendEnable = FALSE;
	bdesc.RenderTarget[0].RenderTargetWriteMask = 0xF;
	g_D3D11Device->CreateBlendState (&bdesc, &g_D3D11BlendState);
}

static void ReleaseD3D11Resources()
{
	SAFE_RELEASE(g_D3D11VB);
	SAFE_RELEASE(g_D3D11CB);
	SAFE_RELEASE(g_D3D11VertexShader);
	SAFE_RELEASE(g_D3D11PixelShader);
	SAFE_RELEASE(g_D3D11InputLayout);
	SAFE_RELEASE(g_D3D11RasterState);
	SAFE_RELEASE(g_D3D11BlendState);
	SAFE_RELEASE(g_D3D11DepthState);
}

static void SetGraphicsDeviceD3D11 (ID3D11Device* device, GfxDeviceEventType eventType)
{
	g_D3D11Device = device;

	if (eventType == kGfxDeviceEventInitialize)
		CreateD3D11Resources();
	if (eventType == kGfxDeviceEventShutdown)
		ReleaseD3D11Resources();
}

#endif // #if SUPPORT_D3D11

// -------------------------------------------------------------------
//  Direct3D 9 setup/teardown code


#if SUPPORT_D3D9

static IDirect3DDevice9* g_D3D9Device;

// A dynamic vertex buffer just to demonstrate how to handle D3D9 device resets.
static IDirect3DVertexBuffer9* g_D3D9DynamicVB;

static void SetGraphicsDeviceD3D9 (IDirect3DDevice9* device, GfxDeviceEventType eventType)
{
	g_D3D9Device = device;

	// Create or release a small dynamic vertex buffer depending on the event type.
	switch (eventType) {
	case kGfxDeviceEventInitialize:
	case kGfxDeviceEventAfterReset:
		// After device is initialized or was just reset, create the VB.
		if (!g_D3D9DynamicVB)
			g_D3D9Device->CreateVertexBuffer (1024, D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC, 0, D3DPOOL_DEFAULT, &g_D3D9DynamicVB, NULL);
		break;
	case kGfxDeviceEventBeforeReset:
	case kGfxDeviceEventShutdown:
		// Before device is reset or being shut down, release the VB.
		SAFE_RELEASE(g_D3D9DynamicVB);
		break;
	}
}

#endif // #if SUPPORT_D3D9



extern "C" void EXPORT_API UnitySetGraphicsDevice (void* device, int deviceType, int eventType)
{
	// Set device type to -1, i.e. "not recognized by our plugin"
	g_DeviceType = -1;
	
	#if SUPPORT_D3D9
	// D3D9 device, remember device pointer and device type.
	// The pointer we get is IDirect3DDevice9.
	if (deviceType == kGfxRendererD3D9)
	{
		DebugLog ("Set D3D9 graphics device\n");
		g_DeviceType = deviceType;
		SetGraphicsDeviceD3D9 ((IDirect3DDevice9*)device, (GfxDeviceEventType)eventType);
	}
	#endif

	#if SUPPORT_D3D11
	// D3D11 device, remember device pointer and device type.
	// The pointer we get is ID3D11Device.
	if (deviceType == kGfxRendererD3D11)
	{
		DebugLog ("Set D3D11 graphics device\n");
		g_DeviceType = deviceType;
		SetGraphicsDeviceD3D11 ((ID3D11Device*)device, (GfxDeviceEventType)eventType);
	}
	#endif

	#if SUPPORT_OPENGL
	// If we've got an OpenGL device, remember device type. There's no OpenGL
	// "device pointer" to remember since OpenGL always operates on a currently set
	// global context.
	if (deviceType == kGfxRendererOpenGL)
	{
		DebugLog ("Set OpenGL graphics device\n");
		g_DeviceType = deviceType;
	}
	#endif
}




// --------------------------------------------------------------------------
// SetDefaultGraphicsState
//
// Helper function to setup some "sane" graphics state. Rendering state
// upon call into our plugin can be almost completely arbitrary depending
// on what was rendered in Unity before.
// Before calling into the plugin, Unity will set shaders to null,
// and will unbind most of "current" objects (e.g. VBOs in OpenGL case).
//
// Here, we set culling off, lighting off, alpha blend & test off, Z
// comparison to less equal, and Z writes off.

static void SetDefaultGraphicsState ()
{
		#if SUPPORT_D3D9
	// D3D9 case
	if (g_DeviceType == kGfxRendererD3D9)
	{
		g_D3D9Device->SetRenderState (D3DRS_CULLMODE, D3DCULL_NONE);
		g_D3D9Device->SetRenderState (D3DRS_LIGHTING, FALSE);
		g_D3D9Device->SetRenderState (D3DRS_ALPHABLENDENABLE, FALSE);
		g_D3D9Device->SetRenderState (D3DRS_ALPHATESTENABLE, FALSE);
		g_D3D9Device->SetRenderState (D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
		g_D3D9Device->SetRenderState (D3DRS_ZWRITEENABLE, FALSE);
	}
	#endif


	#if SUPPORT_D3D11
	// D3D11 case
	if (g_DeviceType == kGfxRendererD3D11)
	{
		ID3D11DeviceContext* ctx = NULL;
		g_D3D11Device->GetImmediateContext (&ctx);
		ctx->OMSetDepthStencilState (g_D3D11DepthState, 0);
		ctx->RSSetState (g_D3D11RasterState);
		ctx->OMSetBlendState (g_D3D11BlendState, NULL, 0xFFFFFFFF);
		ctx->Release();
	}
	#endif


	#if SUPPORT_OPENGL
	// OpenGL case
	if (g_DeviceType == kGfxRendererOpenGL)
	{
		glDisable (GL_CULL_FACE);
		glDisable (GL_LIGHTING);
		glDisable (GL_BLEND);
		glDisable (GL_ALPHA_TEST);
		glDepthFunc (GL_LEQUAL);
		glEnable (GL_DEPTH_TEST);
		glDepthMask (GL_FALSE);
	}
	#endif
}



// --------------------------------------------------------------------------
// UnityRenderEvent
// This will be called for GL.IssuePluginEvent script calls; eventID will
// be the integer passed to IssuePluginEvent. In this example, we just ignore
// that value.

extern "C" void EXPORT_API UnityRenderEvent (int eventID)
{

	if (g_DeviceType == kGfxRendererOpenGL) //edited: now if opengl, dont do anything. 
		return;

	SetDefaultGraphicsState();
	if(eventID == 1){
			

	#if SUPPORT_D3D9
	// D3D9 case
	if (g_DeviceType == kGfxRendererD3D9)
	{

		//un-implemented d3d9 code for now, as i didn't need it.

//		// Update native texture from code
//		if (allPlayersTexturePtr)
//		{
//			IDirect3DTexture9* d3dtex = (IDirect3DTexture9*)allPlayersTexturePtr;
//			D3DSURFACE_DESC desc;
//			d3dtex->GetLevelDesc (0, &desc);
//			D3DLOCKED_RECT lr;
//			d3dtex->LockRect (0, &lr, NULL, 0);
//			memcpy(lr.pBits, start_ptr, desc.Width * desc.Height);
//			d3dtex->UnlockRect (0);
//		}
//
//		if (everyoneElsePtr)
//		{
//			IDirect3DTexture9* d3dtex = (IDirect3DTexture9*)everyoneElsePtr;
//			D3DSURFACE_DESC desc;
//			d3dtex->GetLevelDesc (0, &desc);
//			D3DLOCKED_RECT lr;
//			d3dtex->LockRect (0, &lr, NULL, 0);
//			memcpy(lr.pBits, start_ptr  + sharedMemorySectionLength, desc.Width * desc.Height);
//			d3dtex->UnlockRect (0);
//		}
	}
	#endif


	#if SUPPORT_D3D11
	// D3D11 case
	//if (g_DeviceType == kGfxRendererD3D11 && !g_D3D11VertexShader)
	//	DebugLog("not renderin, no vertex shader for d3d11");
	if (g_DeviceType == kGfxRendererD3D11 && g_D3D11VertexShader)
	{
		ID3D11DeviceContext* ctx = NULL;
		g_D3D11Device->GetImmediateContext (&ctx);
		// update native texture from code
		if (bodyIndexFrameTexturePtr)
		{
			ID3D11Texture2D* d3dtex = (ID3D11Texture2D*)bodyIndexFrameTexturePtr;
			D3D11_TEXTURE2D_DESC desc;
			d3dtex->GetDesc (&desc);
			ctx->UpdateSubresource(d3dtex, 0, NULL, bodyIndexFrameTexturePtrSourceDataLocation, desc.Width, 0);
			//DXGI_FORMAT_R8_TYPELESS
		}//

		//unused- using ComputeBuffers/ComputeShaders instead for this texture
		if (depthFrameTexturePtr)
		{
			ID3D11Texture2D* d3dtex = (ID3D11Texture2D*)depthFrameTexturePtr;
			D3D11_TEXTURE2D_DESC desc;
			d3dtex->GetDesc (&desc);
			ctx->UpdateSubresource(d3dtex, 0, NULL, depthFrameTexturePtrSourceDataLocation, desc.Width, 0);
			//std::string s = DebugStringD3D11(desc);
			//depth with 16 bit depth buffer:
			//float desc : Width : 512 Height : 424 MipLevels : 1 ArraySize : 1 Format : R16_TYPELESS(53) SampleCount : 1 SampleQuality : 0 Usage : DEFAULT(0) BindFlags : +SHADER_RESOURCE + DEPTH_STENCIL(48) CPUAccessFlags : (0) MiscFlags : (0)
			
			//RFloat in unity:
			//float desc:  Width:512 Height:424 MipLevels:1 ArraySize:1 Format:R32_TYPELESS(39) SampleCount:1 SampleQuality:0 Usage:DEFAULT(0) BindFlags:+SHADER_RESOURCE+RENDER_TARGET(28) CPUAccessFlags:(0) MiscFlags:(0)

			//DXGI_FORMAT_R16_TYPELESS is RHalf
			//DXGI_FORMAT_R32_TYPELESS is RFloat
			//DXGI_FORMAT_R32G32B32A32_TYPELESS is ARGBFloat
//			DebugLog(("float desc: " +  s ).c_str());
		}	

		ctx->Release();
	}
	#endif
	}
}
