# README #

**This is preliminary software and/or hardware and APIs are preliminary and subject to change.
**
This is a fork of Microsoft's April SDK Update for Unity with fast-to-texture depth + body index frame + skeleton grabbing.



Requires:

* 1) Unity Pro, 

* 2) directx11 mode, as per file/build settings / other settings / 'use direct3d 11' (requires a dx11-capable graphics card)

* 3) set the nvidia control panel / manage 3d settings / preferred graphics processor: high-performance graphics processor.