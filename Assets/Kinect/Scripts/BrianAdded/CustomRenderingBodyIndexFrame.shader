﻿Shader "Custom/RenderingBodyIndexFrame" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_BodyIndicesList1 ("_BodyIndicesList1", Vector) = (-1,-1,-1,-1)  
		_BodyIndicesList2 ("_BodyIndicesList2", Vector) = (-1,-1,-1,-1)  

	}
	SubShader {
		Pass {

ZTest Always Cull Off ZWrite Off
Fog { Mode off }
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma enable_d3d11_debug_symbols

			#ifndef SHADER_API_D3D11
				#pragma target 3.0
			#else
				#pragma target 4.0
			#endif

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform float4 _BodyIndicesList1;
			uniform float2 _BodyIndicesList2;
			bool floatingCompare(float someFloat, float anotherFloat){
				if (abs(someFloat - anotherFloat) < .0001){
				return true;
				}
				else
				return false;
			}

			float4 modifiedColor(float4 inputColor)
			{
				float color255 = inputColor * 255.0;
				if(floatingCompare(_BodyIndicesList1.x, color255) || 
				floatingCompare(_BodyIndicesList1.y, color255) || 
				floatingCompare(_BodyIndicesList1.z, color255) || 
				floatingCompare(_BodyIndicesList1.w, color255) || 
				floatingCompare(_BodyIndicesList2.x, color255) || 
				floatingCompare(_BodyIndicesList2.y, color255) 
				 )
				{
					//but if they're equal, make it a shadow.
					return float4(0.0, 0.0, 0.0, 0.0);
				}
				//mostly white
				else
				return float4(1.0, 1.0, 1.0,1.0);
			}

			float4 frag(v2f_img i) : COLOR {
				float2 uvs =  i.uv;
				uvs.x = 1.0 -uvs.x;
				return modifiedColor(tex2D(_MainTex, uvs));
			}
			ENDCG
		}

	}
}