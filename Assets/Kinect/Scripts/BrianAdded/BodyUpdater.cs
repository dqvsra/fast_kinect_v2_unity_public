﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.InteropServices;
using Kinect = Windows.Kinect;
using Windows.Kinect;

public class BodyUpdater : MonoBehaviour {
    private KinectSensor _Sensor;
    private CoordinateMapper _Mapper;

    public JointWrapper[] BodyParts;
	public int prefabIndexID;
    public int BodyIndexMaskID = -1;
    public bool mapJointsToDepthMap = true;
	private Dictionary<Kinect.JointType, GameObject> BodyPartsDictionary = new Dictionary<Kinect.JointType, GameObject>();
	private Dictionary<Kinect.JointType, LineRenderer> LineRendererDictionary = new Dictionary<Kinect.JointType, LineRenderer>();
	/// <summary>
	/// this maps source joints to target joints.
	/// </summary>
	private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
	{
		{ Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
		{ Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
		{ Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
		{ Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },
		
		{ Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
		{ Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
		{ Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
		{ Kinect.JointType.HipRight, Kinect.JointType.SpineBase },
		
		{ Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
		{ Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
		{ Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
		{ Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
		{ Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
		{ Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },
		
		{ Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
		{ Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
		{ Kinect.JointType.HandRight, Kinect.JointType.WristRight },
		{ Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
		{ Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
		{ Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },
		
		{ Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
		{ Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
		{ Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
		{ Kinect.JointType.Neck, Kinect.JointType.Head },
	};

	// Use this for initialization
	void Awake () {
		BodyPartsDictionary = new Dictionary<Kinect.JointType, GameObject>();
		for (int i =0; i < BodyParts.Length; i++) {
			BodyPartsDictionary.Add(BodyParts[i].jointType, BodyParts[i].joint);
			LineRendererDictionary.Add(BodyParts[i].jointType, BodyParts[i].joint.GetComponent<LineRenderer>());
		}
        
        _Sensor = KinectSensor.GetDefault();
        if (_Sensor != null)
        {
            _Mapper = _Sensor.CoordinateMapper;
        }
	}

    void Start()
    {
        for (int i = 0; i < BodyParts.Length; i++)
        {
            if (mapJointsToDepthMap)
                BodyParts[i].joint.transform.localScale = new Vector3(.1f, .1f, .1f);
        }
    }

	public void InitWithPrefabIndex(int idx){
		prefabIndexID = idx;
	}

	private static Color GetColorForState(Kinect.TrackingState state)
	{
		switch (state)
		{
		case Kinect.TrackingState.Tracked:
			return Color.green;
			
		case Kinect.TrackingState.Inferred:
			return Color.red;
			
		default:
			return Color.black;
		}
	}
	
	private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
	{
		return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
	}


	// 'body' is the raw data. 'bodyObject' is this gameobject that has joint children...
    public void RefreshBodyObject(Kinect.Body body)
    {
        Dictionary<Windows.Kinect.JointType, Windows.Kinect.Joint> jointz = body.Joints;
        DepthSpacePoint[] depthPoints = new DepthSpacePoint[jointz.Count];
        CameraSpacePoint[] cameraPoints = new CameraSpacePoint[jointz.Count];
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = jointz[jt];
            cameraPoints[(int)jt] = jointz[jt].Position;

            //if you want to map the joints to the depth map, don't apply the transform to the source joint
            if (mapJointsToDepthMap)
                continue;

            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = jointz[_BoneMap[jt]];
            }

            if (BodyPartsDictionary.ContainsKey(jt))
            {
            }
            else
            {
                Debug.Log("doesnt contain key: " + jt);
            }
            Transform jointObj = BodyPartsDictionary[jt].transform;
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            //line renderer stuff for trackingState color
            LineRenderer lr = LineRendererDictionary[jt];
            if (targetJoint.HasValue && lr != null && lr.enabled)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }

        }
        _Mapper.MapCameraPointsToDepthSpace(cameraPoints, depthPoints);

        if (mapJointsToDepthMap)
        {
            float w = DepthSourceManager.Instance.FrameWidth;
            float h = DepthSourceManager.Instance.FrameHeight;
            for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
            {
                Kinect.Joint sourceJoint = jointz[jt];
                Kinect.Joint? targetJoint = null;
                Transform targetTransform = null;
                //this is what each source joint 'connects' to
                if (_BoneMap.ContainsKey(jt))
                {
                    targetJoint = jointz[_BoneMap[jt]];
                    targetTransform = BodyPartsDictionary[_BoneMap[jt]].transform;
                }

                Transform jointObj = BodyPartsDictionary[jt].transform;

                Vector3 jointPosVector = new Vector3((10.0f * depthPoints[(int)jt].X / w) - 5.0f, (10.0f * (h - depthPoints[(int)jt].Y) / h) - 5.0f, 0);
                if(!float.IsNaN(jointPosVector.x) && !float.IsNaN(jointPosVector.y) && !float.IsInfinity(jointPosVector.x) && !float.IsInfinity(jointPosVector.y))
                    jointObj.localPosition = new Vector3( (10.0f * depthPoints[(int)jt].X / w) -5.0f,( 10.0f * (h - depthPoints[(int)jt].Y) / h) - 5.0f, 0);

                //line renderer stuff for trackingState color
                LineRenderer lr = LineRendererDictionary[jt];
                if (targetJoint.HasValue && lr != null && lr.enabled)
                {
                    lr.SetPosition(0, jointObj.position);
                    lr.SetPosition(1, targetTransform.position);
                    lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
                    lr.useWorldSpace = true;
                }
            }
        }
    }


}
