﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//the body index mask shader relies on the skeleton data (the BodyFrame data) to know which users are 'valid' - you should be able to pick and choose which users
//you want to see.
public class BodyIndexFrameView : MonoBehaviour {
    public BodySourceView bodySourceView;
    public GameObject bodyIndexRenderer;
    public Material bodyIndexMaskMaterial;
    private static BodyIndexFrameView instance;
    public static BodyIndexFrameView Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject();
                go.name = "_BodyIndexFrameView(Singleton)";
                instance = go.AddComponent<BodyIndexFrameView>();
            }
            return instance;
        }
    }
  

    void Awake()
    {
        //if it's null or if it's the right instance, don't destroy it.
        if (instance == null || instance == this)
            instance = this;
        else
        {
            DestroyImmediate(this);
        }
    }
	// Use this for initialization
	void Start () {
        bodyIndexRenderer.renderer.material.mainTexture = BodyIndexFrameSourceManager.Instance.GetBodyIndexFrameTexture();
	}
	
	// Update is called once per frame
	void Update () {
        ApplyVisibleBodyIndicesMask();
    }

    public void ApplyVisibleBodyIndicesMask()
    {
        List<float> bodyIndices = bodySourceView.GetUsedBodyIndices();
        Vector4 firstSet = new Vector4(-1, -1, -1, -1);
        Vector4 secondSet = new Vector4(-1, -1, -1, -1);
        for (int i = 0; i < bodyIndices.Count; i++)
        {
            switch (i)
            {
                case 0:
                    firstSet.x = bodyIndices[i];
                    break;
                case 1:
                    firstSet.y = bodyIndices[i];
                    break;
                case 2:
                    firstSet.z = bodyIndices[i];
                    break;
                case 3:
                    firstSet.w = bodyIndices[i];
                    break;
                case 4:
                    secondSet.x = bodyIndices[i];
                    break;
                case 5:
                    secondSet.y = bodyIndices[i];
                    break;
            }
        }

         bodyIndexRenderer.renderer.material.SetVector("_BodyIndicesList1", firstSet);
         bodyIndexRenderer.renderer.material.SetVector("_BodyIndicesList2", secondSet);
    }
}
