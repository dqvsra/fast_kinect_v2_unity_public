﻿using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using Windows.Kinect;
using RootSystem = System;
using System.IO;
using System;

public class BodyIndexFrameSourceManager : MonoBehaviour
{   

	[RootSystem.Runtime.InteropServices.DllImport("RenderingPlugin", CallingConvention=RootSystem.Runtime.InteropServices.CallingConvention.Cdecl)]
	private static extern void SetupBodyIndexFrameTexture (int w, int h, IntPtr _texturePtr, IntPtr _bodyIndexFrameTexturePtrSourceDataLocation);
    [RootSystem.Runtime.InteropServices.DllImport("RenderingPlugin", CallingConvention = RootSystem.Runtime.InteropServices.CallingConvention.Cdecl)]
    private static extern void CleanupBodyIndexFrameTexture();

    private KinectSensor _Sensor;
    private BodyIndexFrameReader _Reader;
    //this is gonna get pinned
    private byte[] _Data;
	public int FrameWidth = 0;
    public int FrameHeight = 0;
    public uint LengthInPixels = 0;
	public RenderTexture _Texture;
	private GCHandle gcHandle;
    private static BodyIndexFrameSourceManager instance;
    public static BodyIndexFrameSourceManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject();
                go.name = "_BodyIndexFrameSourceManager(Singleton)";
                instance = go.AddComponent<BodyIndexFrameSourceManager>();
            }
            return instance;
        }
    }
  

	public RenderTexture GetBodyIndexFrameTexture()
	{
		return _Texture;
	}

    public byte[] GetData()
    {
	        return _Data;
    }

    void Awake () 
    {
        //if it's null or if it's the right instance, don't destroy it.
        if (instance == null || instance == this)
            instance = this;
        else
        {
            DestroyImmediate(this);
        }

        _Sensor = KinectSensor.GetDefault();
        
        if (_Sensor != null) 
        {
            _Reader = _Sensor.BodyIndexFrameSource.OpenReader();
			Windows.Kinect.FrameDescription frameDesc = _Sensor.BodyIndexFrameSource.FrameDescription;
			FrameWidth = frameDesc.Width;
			FrameHeight = frameDesc.Height;
            LengthInPixels = frameDesc.LengthInPixels;
            _Data = new byte[LengthInPixels];
            _Texture = new RenderTexture(FrameWidth, FrameHeight, 0, RenderTextureFormat.R8);
            _Texture.wrapMode = TextureWrapMode.Clamp;
            _Texture.filterMode = FilterMode.Point;
            _Texture.Create();
            gcHandle = GCHandle.Alloc(_Data, GCHandleType.Pinned);
			SetupBodyIndexFrameTexture(FrameWidth, FrameHeight, _Texture.GetNativeTexturePtr(), (IntPtr)gcHandle.AddrOfPinnedObject());
			if (!_Sensor.IsOpen)
			{
				_Sensor.Open();
			}
        }
    }
    
    void Update () 
    {
        if (_Reader != null)
        {
            var frame = _Reader.AcquireLatestFrame();
            if (frame != null)
            {
                frame.CopyFrameDataToArray(_Data);
                frame.Dispose();
                GL.IssuePluginEvent(1);
                frame = null;
            }
        }
    }
    
    void OnApplicationQuit()
    {
        CleanupBodyIndexFrameTexture();

		if(gcHandle.IsAllocated)
		gcHandle.Free ();

        if (_Reader != null)
        {
            _Reader.Dispose();
            _Reader = null;
        }
        
        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }
            
            _Sensor = null;
        }
    }
}
